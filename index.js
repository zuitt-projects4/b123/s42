/*
	JS DOM (Document Object Model)

	In CSS, we had a concept of a Box Model. Every element is considered by CSS as a box. 
	In JS, we have Document Object Model.
		This allows to manipulate our HTML elements with Javascript. Because for JS, HTML elements are all considered as Objects.

	In such a case, our elements as objects, we can then access and manipulate the properties of an element. 

	We can actually do a lot of the things with JS DOM, in fact you've seen a sample before wherein a button is clicked the style of an element changed. For our JS DOM session we will focus mainly on the use of Forms.
	
	document refers to the whole page.
	.querySelector() - is a method that can be usesd to select a specific element from our document. The querySelector uses CSS like selector to select an element.

	console.log(firstNameLabel) - we were able to select an element by its id from our document and saved it in a variable.

	.innerHTML - is a property of an element which considers all the children of the selected element as a string. This includes other elements and text content. 

	firstNameLabel.innerHTML = "I Like New York City." - we reassigned the value of the innerHTML property of our firstNameLabel.
*/

console.log(document)


let firstNameLabel = document.querySelector("#label-first-name")

console.log(firstNameLabel.innerHTML)

firstNameLabel.innerHTML = "I Like New York City."

//mini-act

let lastNameLabel = document.querySelector("#label-last-name")
lastNameLabel.innerHTML = "My Favorite food is lasagna."


let city = "Tokyo"

if (city === "New York"){

	firstNameLabel.innerHTML = `I like New York City`
}else{

	firstNameLabel.innerHTML = `I don't like New York City. I like ${city} City.`
}


/*
	Events - allows us to add  interactivity to our page. Wherein, we can have our users interact with a page and our page can then perform a task.

	EVENT LISTENERS
		eventlisteners - allows us to listen or detect an interaction between the user and the page. On the event that the user clicks, presses a key or hover selected element, we will perform a function.

		
 
*/

firstNameLabel.addEventListener('click', () =>{

	firstNameLabel.innerHTML = "I've been clicked, send HELP!"
	firstNameLabel.style.color = "red"
	firstNameLabel.style.fontSize = "10vh"
})

/*
	ELements have a property called style which is the style of an element style in JS is also an object with properties. 
		like color and fontSize.
*/

lastNameLabel.addEventListener('click', () =>{

	if(lastNameLabel.style.color === "purple"){
		
		lastNameLabel.style.color = ""
		lastNameLabel.style.fontSize = ""

	}else{

		lastNameLabel.style.color = "purple"
		lastNameLabel.style.fontSize = "5vh"

	}

})

/*
	keyup - is an event wherein we are able to perform a task when a user lets go of a key. Keyup is best used in input elements that require key inputs.

	.value = is a property of mostly input element which contains the current value in the input element.

	initial value of your input element

	console.log() ran only the first time our page loaded and does not ran again.

	syntax: element.addEventListener(<event>, <function>)

	you can create multiple event listeners that run the same function

*/

let inputFirstName = document.querySelector("#txt-first-name")

let fullNameDisplay  = document.querySelector("#full-name-display")
/*console.log(inputFirstName.value)*/

const showName = () => {

	fullNameDisplay.innerHTML = inputFirstName.value + " " +inputLastName.value

}

inputFirstName.addEventListener('keyup', showName)

let inputLastName = document.querySelector("#txt-last-name")

inputLastName.addEventListener('keyup', showName)
